# Sensorbucket Chart

Helm chart to install the SensorBucket system in Kubernetes!

## Requirements

- The `sb-istio` chart must be installed
- A Postgresql instance must be available
  - For example with the following install command:  
    ```
    helm install postgresql stable/postgresql --set 'image.repository=mdillon/postgis,image.tag=latest,postgresqlUsername=sensorbucket,postgresqlPassword=sensorbucket,postgresqlDatabase=sensorbucket'
    ```